
#define WHAT "SensorMesh2: create a single .ply mesh for all the specified time interval using sensor topology"

#include <ctime>
#include <iostream>
#include <limits>
#include "libXMls/XMls.h"
#include "libXBase/XPt2D.h"


using namespace std;

string Ext(string filename)
{
	string format = filename.substr(filename.size()-3,3);
	cout << "format: " << format << endl;
	return format;
}
    
struct Triangle
{
    unsigned int i,j,k; // pulse with return idx
    Triangle(unsigned int i_=0, unsigned int j_=0, unsigned int k_=0):
        i(i_), j(j_), k(k_) {}
};

template <typename T> void Write(char * & it, T data)
{
    *reinterpret_cast<T*>(it) = data;
    it += sizeof(T);
}

void WritePly(XMls & mls, vector<Triangle> v_tri,
              int n_vertex, ParamSet & params)
{
	string output_file = params.Get<string>("o");
    ofstream fileOut(output_file);
    if(!fileOut.good())
    {
        cout << "Cannot open " + output_file + " for writing\n";
        return;
    }
    XPt3D Pivot(params.Get<double>("pe"),params.Get<double>("pn"),params.Get<double>("ph"));
    unsigned add_rgb = params.Get<unsigned>("ar");

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << params.Get<string>("ept") <<
    " secs " << params.Get<unsigned>("is") << " to " << params.Get<unsigned>("ie") << endl;
    fileOut << "comment IGN offset Pos " << Pivot.X << " " << Pivot.Y << " " << Pivot.Z << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    if(add_rgb == 1)
    {
        fileOut << "property uchar red" << endl;
        fileOut << "property uchar green" << endl;
        fileOut << "property uchar blue" << endl;
    } else if(add_rgb == 2)
    {
        fileOut << "property float quality" << endl;
    }
    fileOut << "element face " << v_tri.size() << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_bytesize = 3*sizeof(float);
    unsigned long vertex_buffer_size = vertex_bytesize * n_vertex;
    XFloatAttrib * p_refl=NULL;
    if(add_rgb > 0) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(add_rgb == 1) vertex_buffer_size += 3*sizeof(unsigned char) * n_vertex;
    else if(add_rgb == 2) vertex_buffer_size += sizeof(float) * n_vertex;

    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            XEchoIndex last_echo_idx = mls.IdxLastEcho(block_idx, pulse_idx);
            XPt3D P = mls.Pworld(block_idx, last_echo_idx)-Pivot;
            Write<float>(it, P.X);
            Write<float>(it, P.Y);
            Write<float>(it, P.Z);
            if(add_rgb == 1) // r g b mode
            {
                float g = 12.75f*(p_refl->at(block_idx)[last_echo_idx]+20.f); // rescale [-20,0] to [0,256] TODO: parameters
                if(g<0.f) g=0.f; else if(g>255.f) g=255.f;
                unsigned char ug = g;
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
                Write<unsigned char>(it, ug);
            }
            else if(add_rgb == 2) // quality mode
            {
                Write<float>(it, p_refl->at(block_idx)[last_echo_idx]);
            }
        }
        mls.Free(block_idx);
    }
    cout << "Writing " << n_vertex << " vertices of size " << vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*v_tri.size();
    buffer = new char[tri_buffer_size]; it = buffer;
    for(auto & tri:v_tri)
    {
        Write<unsigned char>(it, 3);
        Write<int>(it, tri.i);
        Write<int>(it, tri.j);
        Write<int>(it, tri.k);
    }
    cout << "Writing " << v_tri.size() << " triangles of size " << tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(vertex_buffer_size+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}

void WriteOff(XMls & mls, vector<Triangle> v_tri,
              int n_vertex, ParamSet & params)
{
	string output_file = params.Get<string>("o");
    ofstream fileOut(output_file);
    if(!fileOut.good())
    {
        cout << "Cannot open " + output_file + " for writing\n";
        return;
    }
    XPt3D Pivot(params.Get<double>("pe"),params.Get<double>("pn"),params.Get<double>("ph"));

    // write text header
    fileOut << "OFF" << endl;
    fileOut << "# Generated from " << params.Get<string>("ept") <<
    " secs " << params.Get<unsigned>("is") << " to " << params.Get<unsigned>("ie") << endl;
    fileOut << "# IGN offset Pos " << Pivot << endl << endl;
    // number of edges is not used in OFF but Meshlab fails if absent. It is long to compute so we provide an estimate (exact for watertight meshes)
    fileOut << n_vertex << " " << v_tri.size() << " " << v_tri.size() << endl;
    // vertex list
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++) if(mls.NbOfEcho(block_idx, pulse_idx)>0)
        {
            XPt3D P = mls.Pworld(block_idx, mls.IdxLastEcho(block_idx, pulse_idx))-Pivot;
            fileOut << P.X << " " << P.Y << " " << P.Z << endl;
        }
        mls.Free(block_idx);
    }

    // triangle list
    for(auto & tri:v_tri) fileOut << "3 " << tri.i << " " << tri.j << " " << tri.k << endl;
    fileOut.close();
}

float MaxEdgeSize(XMls & mls, XBlockIndex b1, XPulseIndex id1, XBlockIndex b2, XPulseIndex id2, XBlockIndex b3, XPulseIndex id3)
{
    XPt3D P1 = mls.Pworld(b1, mls.IdxLastEcho(b1, id1));
    XPt3D P2 = mls.Pworld(b2, mls.IdxLastEcho(b2, id2));
    XPt3D P3 = mls.Pworld(b3, mls.IdxLastEcho(b3, id3));
    double d12 = (P1-P2).Norme();
    double d23 = (P2-P3).Norme();
    double d31 = (P3-P1).Norme();
    return max(d12,max(d23,d31));
}

float MaxEdgeSize(XMls & mls, XBlockIndex b, XPulseIndex id1, XPulseIndex id2, XPulseIndex id3)
{
    return MaxEdgeSize(mls, b, id1, b, id2, b, id3);
}

// make a wedge based on the four vertices ABXY = (AXB)+(ABY) triangles
void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
               XBlockIndex ba, XPulseIndex pa, XBlockIndex bb, XPulseIndex pb,
               XBlockIndex bx, XPulseIndex px, XBlockIndex by, XPulseIndex py)
{
    if(mls.NbOfEcho(ba, pa)==0 || mls.NbOfEcho(bb, pb) == 0) return;
    //cout << "ABXY (" << ba << "," << pa << ")("<< bb << "," << pb << ")("<< bx << "," << px << ")("<< by << "," << py << ")" << endl;
    int A=vv_pulse_with_echo_idx[ba][pa], B=vv_pulse_with_echo_idx[bb][pb];
    int X=vv_pulse_with_echo_idx[bx][px], Y=vv_pulse_with_echo_idx[by][py];
    if(mls.NbOfEcho(bx, px)>0)
    {
        if(MaxEdgeSize(mls, ba, pa, bb, pb, bx, px) < tri_threshold)
            v_tri.push_back(Triangle(A, B, X));
    }
    if(mls.NbOfEcho(by, py)>0)
    {
        if(MaxEdgeSize(mls, ba, pa, bb, pb, by, py) < tri_threshold)
            v_tri.push_back(Triangle(A, Y, B));
    }
}

// mono block version
inline void MakeWedge(XMls & mls, vector<Triangle> & v_tri, vector<vector<int> > & vv_pulse_with_echo_idx, float tri_threshold,
                      XBlockIndex b, XPulseIndex pa, XPulseIndex pb, XPulseIndex px, XPulseIndex py)
{
    MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, tri_threshold, b, pa, b, pb, b, px, b, py);
}

//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    cout << WHAT << endl;
    ParamSet params;
    XMls::AddParam(params);
    params.Add(new TParam<string>("o", "",
    "name of the output mesh file (.ply or .off only)"));
    params.Add(new TParam<int>("f", 0,
    "id of the block to process (look at info.txt in ept_dir for valid range)"));
    params.Add(new TParam<float>("bl", 50.,
    "block_length: expected length of an exported block on min distance between lines to keep it"));
    params.Add(new TParam<float>("DP", 0.,
    "max_DP_error: maximum error for the Douglas Peucker simplification"));
    params.Add(new TParam<float>("tt", 0.5,
    "tri_threshold: threshold on max triangle edge size to add the triangle"));
    params.Add(new TParam<int>("is", -1,
    "i_start: start time of the laser points to export, -1 for first ept block (look at info.txt in ept_dir for valid range)"));
    params.Add(new TParam<int>("ie", -1,
    "i_end: end time of the laser points to export, -1 for last ept block (look at info.txt in ept_dir for valid range)"));
    params.Add(new TParam<unsigned>("ar", false,
    "add_rgb: (0:add nothing, 1: add reflectance in r,g,b attributes, 2:add reflectance in quality, 3:add center coords) in .ply"));
    params.Add(new TParam<double>("pe", -1.,
    "pivot_E: optionally set manually the pivot point, -1. for automatic"));
    params.Add(new TParam<double>("pn", 0.,
    "pivot_N: optionally set manually the pivot point"));
    params.Add(new TParam<double>("ph", 0.,
    "pivot_H: optionally set manually the pivot point"));
    if(argc < 2)
    {
        cout << params.Help() << endl;
        return 0;
    }
    params.Parse(argc, argv);
    XSecond i_start=params.Get<int>("is"), i_end=params.Get<int>("ie");
	double tri_threshold = params.Get<double>("tt");
    //double max_DP_error = params.Get<double>("DP");
    //double block_length = params.Get<double>("bl");
    string output_file = params.Get<string>("o");

    clock_t start = clock();
    // constructor and infos accessible after construction
    XTrajecto traj(params);
    XMls mls(params, &traj);
    mls.Select(i_start, i_end);
    params.Set<int>("is", i_start);
    params.Set<int>("ie", i_end);
    cout << mls.NBlock() << " block(s) selected" << endl;

    if(params.Get<double>("pe") == -1)
    {
        XPt3D Pivot = traj.GetGeoref(0).Translation();
        params.Set<double>("pe", 100*(int)(Pivot.X/100));
        params.Set<double>("pn", 100*(int)(Pivot.Y/100));
    }

    start = clock();
    int PPL = mls.PulsePerLine();
    int idx=0;
    vector< vector<int> > vv_pulse_with_echo_idx(mls.NBlock()); // unique indexation of pulses with at least one echo across blocks
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        // index pulses with at leat one echo
        vv_pulse_with_echo_idx[block_idx] = vector<int>(mls.NPulse(block_idx), -1);
        for(XPulseIndex pulse_idx=0; pulse_idx<mls.NPulse(block_idx); pulse_idx++)
            if(mls.NbOfEcho(block_idx, pulse_idx)>0)
                vv_pulse_with_echo_idx[block_idx][pulse_idx] = idx++;
        mls.Free(block_idx);
    }

    // create triangles list
    vector<Triangle> v_tri;
    mls.Load(0);
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        XPulseIndex n_pulse = mls.NPulse(block_idx);
        for(XPulseIndex pulse_idx=0; pulse_idx<n_pulse-PPL-1; pulse_idx++)
        {
            MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, tri_threshold,
                      block_idx, pulse_idx, pulse_idx+PPL+1, pulse_idx+PPL, pulse_idx+1);
        }
        // triangles between current block and next one
        if(block_idx<mls.NBlock()-1)
        {
            mls.Load(block_idx+1);
            cout << "PPL " << PPL << " n_pulse " << n_pulse << " block " << block_idx << "/" << mls.NBlock() << endl;
            // first wedge (3 vertices in block, 1 in block+1)
            MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, tri_threshold,
                      block_idx, n_pulse-PPL-1, block_idx+1, 0,
                      block_idx, n_pulse-1, block_idx, n_pulse-PPL);
            for(XPulseIndex pulse_idx=n_pulse-PPL; pulse_idx<n_pulse-1; pulse_idx++)
            {
                // strip wedges (2 vertices in block, 2 in block+1)
                MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, tri_threshold,
                          block_idx, pulse_idx, block_idx+1, pulse_idx+PPL+1-n_pulse,
                          block_idx+1, pulse_idx+PPL-n_pulse, block_idx, pulse_idx+1);
            }
            // last wedge (1 vertex in block, 3 in block+1)
            MakeWedge(mls, v_tri, vv_pulse_with_echo_idx, tri_threshold,
                      block_idx, n_pulse-1, block_idx+1, PPL,
                      block_idx+1, PPL-1, block_idx+1, 0);
        }
        mls.Free(block_idx);
    }
    cout << "Writing " << output_file << endl;
    if(Ext(output_file) == "off") WriteOff(mls, v_tri, idx, params);
    else WritePly(mls, v_tri, idx, params);
    cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;
    return 0;
}

