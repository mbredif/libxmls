#pragma once

/// vertices of priority edges
#include "common.h"
#include "LiteGeom/LgPoint3.hpp"

// forward declaration
class PriorityFace3;

// corresponding vertices
class PriorityVertex3
{
public:
    Lg::Point3f P;
    std::vector<PriorityFace3*> mvp_face;
    PriorityVertex3(float x, float y, float z):P(x,y,z),mvp_face(0){}
    int Valence() const	{return mvp_face.size();}
    int onValence() const;
};

std::ostream& operator<<(std::ostream& os, const PriorityVertex3& v);
