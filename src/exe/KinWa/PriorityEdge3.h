#pragma once

/// edges associated to PriorityTriangles

#include "common.h"

// forward declaration
class PriorityFace3;

class PriorityEdge3
{
public:
	float m_weight; // advice: put the normalized length
    std::vector<PriorityFace3*> mvp_face;
    
    PriorityEdge3(float weight=1.):m_weight(weight),mvp_face(0){}
    int Valence() const	{return mvp_face.size();}
    int onValence() const;
    inline float weight() const {return m_weight;}
    inline float energy() const {return m_weight*onValence();}
    void update();
};

std::ostream& operator<<(std::ostream& os, const PriorityEdge3& e);
