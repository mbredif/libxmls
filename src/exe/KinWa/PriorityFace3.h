#pragma once

/// edges with priorities = energy decrease associated to switching its state

//#include "common.h"
#include "PriorityVertex3.h"
#include "PriorityEdge3.h"

typedef std::multimap<float, PriorityFace3*> pface_queue;

class PriorityFace3
{
    pface_queue * mp_queue;
    pface_queue::iterator m_queue_it;

public:
    //int m_idx;
    PriorityVertex3 * mp_v1, * mp_v2, *mp_v3;
    PriorityEdge3 * mp_e1, * mp_e2, *mp_e3;
	float m_weight; // normalized area or something favoring nice shapes or both
    bool m_on; // is the face kept or not
    PriorityFace3(pface_queue * p_queue,
				 PriorityVertex3 * p_v1,
			     PriorityVertex3 * p_v2,
			     PriorityVertex3 * p_v3,
                 PriorityEdge3 * p_e1,
                 PriorityEdge3 * p_e2,
                 PriorityEdge3 * p_e3,
                 float weight = 1.f,
                 bool on=false);
    float priority(bool display = false) const;
    void add(float priority);
    void add(){add(priority());}
    void remove();
    inline bool isValid() const {return m_queue_it!=pface_queue::iterator();}
    float weight() const {return m_weight;}
    void updatePriority();
    void switchState();
};

std::ostream& operator<<(std::ostream& os, const PriorityFace3& e);
