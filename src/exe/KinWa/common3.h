#pragma once

/// common includes and typedefs for KinWa

#include "common.h"
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/algorithm.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Alpha_shape_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/squared_distance_3.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT FT;

typedef CGAL::Triangulation_vertex_base_with_info_3<unsigned int, K> Vb;
typedef CGAL::Triangulation_data_structure_3<Vb> Tds;
typedef K::Point_3  Point;
typedef K::Segment_3  Segment;
typedef CGAL::Delaunay_triangulation_3<K,Tds> DT3;

typedef CGAL::Alpha_shape_vertex_base_3<K> AVb;
typedef CGAL::Alpha_shape_cell_base_3<K>  ACb;
typedef CGAL::Triangulation_data_structure_3<AVb, ACb> ATds;
typedef CGAL::Delaunay_triangulation_3<K,ATds> ADT3;
typedef CGAL::Alpha_shape_3<ADT3>  Alpha_shape_3;
//typedef Alpha_shape_3::Alpha_shape_edges_iterator Alpha_shape_edges_iterator;
