#pragma once

//#include <iostream>
//using namespace std;

namespace {
// edge manifoldness
// isolated edge, border, manifold, increasingly non-manifold
float we[] = {0, 0.5, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
inline float wef(unsigned int i)
{
	if(i>12)
	{
		//cout << "valence=" << i << endl;
		return i-2;
	}
	return we[i];
}
//float dwe[] = {0.5, -0.5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
}
