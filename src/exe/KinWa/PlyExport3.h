#pragma once

/// Export a .ply file from a generic class giving access to individual triangles

#include "common3.h"
#include "PriorityFace3.h"

class AbstractTriSet
{
public:
	std::string m_name;
    AbstractTriSet(std::string name="abstract"):m_name(name){}
    virtual unsigned int Ntri()=0;
    virtual char * Buffer()=0;
    inline unsigned int VertexBufferSize() {return 9*sizeof(float)*Ntri();}
};

//class DTTriSet:public AbstractTriSet
//{
//public:
    //DT3 * mp_dt;
    //DTTriSet(DT3 * p_dt):mp_dt(p_dt){}
    //virtual unsigned int Ntri() {return mp_dt->number_of_faces();}
    //virtual char * Buffer();
//};

class ASTriSet:public AbstractTriSet
{
public:
    Alpha_shape_3 * mp_as;
    Alpha_shape_3::Classification_type m_type;
    unsigned int m_n_tri=0;
    ASTriSet(Alpha_shape_3 * p_as, Alpha_shape_3::Classification_type type, std::string name="alpha");
    virtual unsigned int Ntri() {return m_n_tri;}
    virtual char * Buffer();
};

class PfaceTriSet:public AbstractTriSet
{
public:
    std::vector<PriorityFace3*> mvp_pface;
    unsigned int m_n_tri;
    PfaceTriSet(std::vector<PriorityFace3*> & vp_pface, std::string name="pface");
    virtual unsigned int Ntri() {return m_n_tri;}
    virtual char * Buffer();
};

void WritePly(AbstractTriSet * p_triset, ParamSet & param);
