#pragma once

/// common includes and typedefs for KinWa
#include "common.h"
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/algorithm.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Alpha_shape_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/squared_distance_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT FT;

typedef CGAL::Triangulation_vertex_base_with_info_2<unsigned int, K> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb> Tds;
typedef K::Point_2  Point;
typedef K::Segment_2  Segment;
typedef CGAL::Delaunay_triangulation_2<K,Tds> DT2;

typedef CGAL::Alpha_shape_vertex_base_2<K> AVb;
typedef CGAL::Alpha_shape_face_base_2<K>  AFb;
typedef CGAL::Triangulation_data_structure_2<AVb, AFb> ATds;
typedef CGAL::Delaunay_triangulation_2<K,ATds> ADT2;
typedef CGAL::Alpha_shape_2<ADT2>  Alpha_shape_2;
typedef Alpha_shape_2::Alpha_shape_edges_iterator Alpha_shape_edges_iterator;

struct param_t
{
    std::string ept_dir, output_ply;
    XSecond block_id=-1;
    // reference length used to adimentionate the energy
	// see it as the maximum length budget to include an isolated point
	float ref_length = 0.5f;
	float alpha=2.f; // alpha shape (radius) parameter
};
