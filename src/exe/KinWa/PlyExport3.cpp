
/// Export a .ply file from a generic class giving access to individual triangles

#include "PlyExport3.h"
using namespace std;

inline string Filename(string base, string name){return base + "." + name + ".mesh.ply";}

template <typename T> void Write(char * & it, T data)
{
    *reinterpret_cast<T*>(it) = data;
    it += sizeof(T);
}

template <> void Write<Point>(char * & it, Point P)
{
    Write<float>(it, P.x());
    Write<float>(it, P.y());
    Write<float>(it, P.z());
}

//char * DTTriSet::Buffer()
//{
    //char * buffer = new char[VertexBufferSize()], * it = buffer;
    //DT3::Finite_faces_iterator fit = mp_dt->finite_faces_begin();
    //for(;fit!=mp_dt->finite_faces_end();fit++)
    //{
        //for(int i=0; i<3; i++) Write<Point>(it, fit->vertex(i)->point());
    //}
    //return buffer;
//}

ASTriSet::ASTriSet(Alpha_shape_3 * p_as, Alpha_shape_3::Classification_type type, std::string name):
    AbstractTriSet(name), mp_as(p_as), m_type(type), m_n_tri(0)
{
    // we want to export regular triangles    
    Alpha_shape_3::Finite_facets_iterator fit = mp_as->finite_facets_begin();
    for(;fit!=mp_as->finite_facets_end();fit++)
        if(mp_as->classify(*fit) == m_type) m_n_tri++;
}

char * ASTriSet::Buffer()
{
    char * buffer = new char[VertexBufferSize()], * it = buffer;
    Alpha_shape_3::Finite_facets_iterator fit = mp_as->finite_facets_begin();
    for(;fit!=mp_as->finite_facets_end();fit++)
    {
        if(mp_as->classify(*fit) == m_type)
        {
			for(int i=0; i<4; i++) if(i != fit->second) 
				Write<Point>(it, fit->first->vertex(i)->point());
		}
	}
    return buffer;
}

template <> void Write<PriorityVertex3>(char * & it, PriorityVertex3 pv)
{
    Write<float>(it, pv.P.x());
    Write<float>(it, pv.P.y());
    Write<float>(it, pv.P.z());
}


PfaceTriSet::PfaceTriSet(vector<PriorityFace3*> & vp_pface, std::string name):
    AbstractTriSet(name), mvp_pface(vp_pface), m_n_tri(0)
{
    for(auto & pface_it:mvp_pface) if(pface_it->m_on) m_n_tri++;
}

char * PfaceTriSet::Buffer()
{
    char * buffer = new char[VertexBufferSize()], * it = buffer;
    for(auto & pface_it:mvp_pface) if(pface_it->m_on)
    {
        Write<PriorityVertex3>(it, *pface_it->mp_v1);
        Write<PriorityVertex3>(it, *pface_it->mp_v2);
        Write<PriorityVertex3>(it, *pface_it->mp_v3);
    }
    return buffer;
}

void WritePly(AbstractTriSet * p_triset, ParamSet & params)
{
	string filename = Filename(params.Get<string>("o"), p_triset->m_name);
    ofstream fileOut(filename);
    if(!fileOut.good())
    {
        cout << "Cannot open " + filename + " for writing\n";
        return;
    }

    // write text header
    unsigned int n_face = p_triset->Ntri(), n_vertex = 3*n_face;
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from:" << endl;
    fileOut << "comment ** ept: " << params.Get<string>("ept") << endl;
    fileOut << "comment ** calib: " << params.Get<string>("calib") << endl;
    fileOut << "comment ** sbet: " << params.Get<string>("sbet") << endl;
    fileOut << "comment ** block: " << params.Get<string>("bi") << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "element face " << n_face << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    char * buffer = p_triset->Buffer();
    unsigned int vertex_buffer_size = p_triset->VertexBufferSize();
    cout << "Writing " << n_vertex << " vertices of size " <<
            vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*n_face;
    buffer = new char[tri_buffer_size];
    char * it = buffer;
    for(unsigned int tri_idx=0; tri_idx<n_face; tri_idx++)
    {
        Write<unsigned char>(it, 3);
        for(int i=0; i<3; i++) Write<int>(it, 3*tri_idx+i);
    }
    cout << "Writing " << n_face << " triangles of size " <<
            tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(p_triset->VertexBufferSize()+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}

