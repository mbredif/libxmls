
#include "weight3.h"
#include "PriorityVertex3.h"
#include "PriorityEdge3.h"
#include "PriorityFace3.h"

using namespace std;

PriorityFace3::PriorityFace3(pface_queue * p_queue,
                           PriorityVertex3 * p_v1,
                           PriorityVertex3 * p_v2,
                           PriorityVertex3 * p_v3,
                           PriorityEdge3 * p_e1,
                           PriorityEdge3 * p_e2,
                           PriorityEdge3 * p_e3,
                           float weight,
                           bool on):
    mp_queue(p_queue), m_queue_it(pface_queue::iterator()),
    mp_v1(p_v1), mp_v2(p_v2), mp_v3(p_v3),
    mp_e1(p_e1), mp_e2(p_e2), mp_e3(p_e3),
    m_weight(weight), m_on(on)
{
	//cout << p_v1->mvp_face.size() << "+";
    //cout << p_v2->mvp_face.size() << "+";
    //cout << p_v3->mvp_face.size() << endl;
    p_v1->mvp_face.push_back(this);
    p_v2->mvp_face.push_back(this);
    p_v3->mvp_face.push_back(this);
    if(p_v1 == p_v2 || p_v1 == p_v3 || p_v3 == p_v2)
		cout << "WARNING: adding a face with duplicate vertices !!!" << endl;
    //cout << p_e1->mvp_face.size() << "-";
    //cout << p_e2->mvp_face.size() << "-";
    //cout << p_e3->mvp_face.size() << endl;
    p_e1->mvp_face.push_back(this);
    p_e2->mvp_face.push_back(this);
    p_e3->mvp_face.push_back(this);
    if(p_e1 == p_e2 || p_e1 == p_e3 || p_e3 == p_e2)
		cout << "WARNING: adding a face with duplicate edges !!!" << endl;
}

float PriorityFace3::priority(bool display) const
{
	// old vertex isolation
    int nv1 = mp_v1->onValence();
    int nv2 = mp_v2->onValence();
    int nv3 = mp_v3->onValence();
    int old_iso = (int)(nv1==0)+(int)(nv2==0)+(int)(nv3==0);
    if(display) cout << nv1<< " " << nv2 << " " << nv3 << " " << old_iso << endl;
    
    // old edge manifoldness
    int ne1 = mp_e1->onValence();
    int ne2 = mp_e2->onValence();
    int ne3 = mp_e3->onValence();
    float old_em = mp_e1->weight()*wef(ne1)+
                   mp_e2->weight()*wef(ne2)+
                   mp_e3->weight()*wef(ne3);
    
    if(m_on)
    {
		// number of vertices that become isolated when removing this face
		int n_isolated = (int)(nv1==1)+(int)(nv2==1)+(int)(nv3==1);
        // new edge manifoldness
		float new_em = mp_e1->weight()*wef(ne1-1)+
					   mp_e2->weight()*wef(ne2-1)+
					   mp_e3->weight()*wef(ne3-1);
        if(display) cout << n_isolated << " " << new_em - old_em << " " << - weight() << endl;
        return n_isolated + new_em - old_em - weight();
	}
	// number of vertices that were isolated before adding this face
    int n_isolated = (int)(nv1==0)+(int)(nv2==0)+(int)(nv3==0);
	float new_em = mp_e1->weight()*wef(ne1+1)+
				   mp_e2->weight()*wef(ne2+1)+
				   mp_e3->weight()*wef(ne3+1);
    if(display) cout << -n_isolated << " " << new_em - old_em << " " << weight() << endl;
    return -n_isolated + new_em - old_em + weight();
}

void PriorityFace3::add(float priority)
{
    m_queue_it = mp_queue->insert(std::make_pair(priority, this));
}

void PriorityFace3::remove()
{
    if(isValid()) mp_queue->erase(m_queue_it);
    else std::cout << "Removing a removed pface" << std::endl;
    m_queue_it = pface_queue::iterator();
}

void PriorityFace3::updatePriority()
{
    remove();
    add();
}

void PriorityFace3::switchState()
{
    m_on = !m_on;
    // TODO: slight opti = prevent this from being updated 3 times
    mp_e1->update();
    mp_e2->update();
    mp_e3->update();
}

std::ostream& operator<<(std::ostream& os, const PriorityFace3& f)
{
    os << "w=" << f.weight() << ",pri=" << f.priority() << '|' << (f.m_on?"on":"off");
    return os;
}
