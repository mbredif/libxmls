
#include "PriorityEdge3.h"
#include "PriorityFace3.h"

int PriorityEdge3::onValence() const
{
	// possible optimization: store it and ++ or -- when an adjacent face switches
    int n=0;
    for(auto & p_face:mvp_face) if(p_face->m_on) n++;
    return n;
}

// update switch cost for all faces sharing this edge
void PriorityEdge3::update()
{
	for(auto & p_face:mvp_face) p_face->updatePriority();
}

std::ostream& operator<<(std::ostream& os, const PriorityEdge3& v)
{
    os << "w=" << v.weight() << ':' << v.onValence() << '/' << v.Valence();
    return os;
}

