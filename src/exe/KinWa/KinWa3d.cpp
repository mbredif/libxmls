
#define WHAT "Create a 3D mesh from a point cloud in ept"

#include "weight3.h"
#include "PlyExport3.h"
#include "libXMls/XEchoPulseTables.h"
#include <random>  

using namespace std;

typedef Alpha_shape_3::Vertex AsVertex;
typedef Alpha_shape_3::Edge AsEdge;
typedef Alpha_shape_3::Vertex_handle AsVertexHandle;
//typedef Alpha_shape_3::Edge_handle AsEdgeHandle;
typedef Alpha_shape_3::Finite_vertices_iterator AsVertexIt;
typedef Alpha_shape_3::Finite_edges_iterator AsEdgeIt;
typedef Alpha_shape_3::Finite_facets_iterator AsFacetIt;
typedef Alpha_shape_3::Edge AsEdge;
typedef Alpha_shape_3::Facet AsFacet;

typedef Alpha_shape_3::Cell_handle                          Cell_handle;
typedef Alpha_shape_3::Vertex_handle                        Vertex_handle;
typedef Alpha_shape_3::Facet                                Facet;
typedef Alpha_shape_3::Edge                                 Edge;

class Energy
{
public:
	vector<PriorityVertex3*> mvp_vertex;
	vector<PriorityEdge3*> mvp_edge;
	vector<PriorityFace3*> mvp_face;
	double mEv, mEe, mEf;
	Energy():mvp_vertex(0), mvp_edge(0), mvp_face(0), mEv(0.), mEe(0.), mEf(0.){};
	
	// isolated vertex energy
	double Ev(bool recompute=true)
	{
		if(recompute)
		{
			unsigned int n_iv=0.;
			for(auto & ip_vertex_it:mvp_vertex)
				n_iv += (ip_vertex_it->onValence()==0);
			mEv = n_iv;
		}
		return mEv;
	}

	// edge manifoldness energy
	double Ee(bool recompute=true)
	{
		if(recompute)
		{
			mEe=0.;
			for(auto & ip_edge:mvp_edge)
			{
				//cout << ip_edge->Valence() << " " << flush;
				mEe += ip_edge->weight()*wef(ip_edge->onValence());
			}
		}
		return mEe;
	}

	// face area energy
	double Ef(bool recompute=true)
	{
		if(recompute)
		{
			mEf=0.;
			for(auto & ip_face:mvp_face) if(ip_face->m_on)
				mEf += ip_face->weight();
		}
		return mEf;
	}
	
	// total energy
	double E(bool recompute=true)
	{
		return Ev(recompute)+Ee(recompute)+Ef(recompute);
	}
	void Dispay(bool recompute=true)
	{
		// subtle: E(recompute) is evaluated first, so no need to recompute the other 3
		cout << "Ev=" << Ev(false) <<
		        " Ee=" << Ee(false) <<
		        " Ef=" << Ef(false) << flush;
		cout << " E=" << E(recompute) << endl;
	}
};

int main(int argc, char **argv)
{
    cout << WHAT << endl;
	ParamSet params;
    XMls::AddParam(params);
    params.Add(new TParam<string>("o", "test", "base name for output .ply files"));
    params.Add(new TParam<int>("f", -1,
    "id of the block to process (look at info.txt in ept_dir for valid range)"));
    params.Add(new TParam<float>("rbl", 0.5f,
    "ref_boundary_length: reference boundary length (in m) used to adimentionate the energy\n\
    see it as the maximum boundary edge length budget to include an isolated point"));
    params.Add(new TParam<float>("ra", 0.5f,
    "ref_area: reference area used to adimentionate the energy\n\
    see it as the maximum boundary edge length budget to include an isolated point"));
    params.Add(new TParam<float>("a", 2.f,
    "alpha: alpha shape (radius) parameter"));
    params.Add(new TParam<float>("p", 0.2f,
    "proportion of the points to process in [0, 1]"));
    params.Add(new TParam<bool>("s", false,
    "0: process the ept, 1: run on synthetic data"));
    params.Add(new TParam<double>("pe", 0.,
    "pivot_E: optionally set manually the pivot point, 0. for automatic"));
    params.Add(new TParam<double>("pn", 0.,
    "pivot_N: optionally set manually the pivot point"));
    params.Add(new TParam<double>("ph", 0.,
    "pivot_H: optionally set manually the pivot point"));
    if(argc < 2)
    {
        cout << params.Help() << endl;
        return 0;
    }
    params.Parse(argc, argv);
    //double dist_threshold = params.Get<double>("dt");
    //double tri_threshold = params.Get<double>("tt");
    //double block_length = params.Get<double>("bl");
    //string output = params.Get<string>("o");

    clock_t start = clock();
    // build PriorityFace/Edge/Vertex3 structures
    Energy E;
    pface_queue pqueue;
    {
		vector<Point> v_pt;
		if(params.Get<bool>("s")) // synthetic mode
				{
			float ni = 32, di=0.05, dj=0.09, nj=ni*di/dj; // sizing parameters
			default_random_engine dre;
			normal_distribution<double> gauss(0.f,0.001f);
			for(int i=1; i<ni; i++) for(int j=1; j<nj; j++)
			{
				if(!(i>=ni/8 && i<=ni/4 && j>=nj/8 && j<=nj/4))
				{
					float x = i*di+gauss(dre), y = j*dj+gauss(dre), z=gauss(dre);
					v_pt.push_back(Point(x,y,z));
					v_pt.push_back(Point(z,x,y));
					v_pt.push_back(Point(y,z,x));
					x = (i+0.5f)*di+gauss(dre); y = (j+0.5f)*dj+gauss(dre); z=gauss(dre);
					v_pt.push_back(Point(x,y,z));
					v_pt.push_back(Point(z,x,y));
					v_pt.push_back(Point(y,z,x));
				}
			}
		}
		else // XMLS mode
		{
			XTrajecto traj(params);
			XMls mls(params, &traj);
			XSecond block_id = params.Get<int>("bi");
			mls.Select(block_id);
			cout << mls.NBlock() << " block(s) selected" << endl;
			mls.Load(0); // load the first (and only) block
			unsigned int n_echo = params.Get<float>("p")*mls.NEcho(0);
			XPt3D P0 = mls.Pworld(0, 0), Pcur=P0;
			v_pt.push_back(Point(P0.X,P0.Y,P0.Z));
			float d2min = 0.05*0.05;
			for(XEchoIndex i_echo=1; i_echo<n_echo; i_echo++)
			{
				XPt3D P = mls.Pworld(0, i_echo)-P0;
				if(dist2(P,Pcur) > d2min)
				{ 
					v_pt.push_back(Point(P.X,P.Y,P.Z));
					Pcur = P;
				}
			}
		}

        cout << "Computing alpha shape for " << v_pt.size() << " points" << endl;
        Alpha_shape_3 A(v_pt.begin(), v_pt.end(), FT(params.Get<float>("a")),
                        Alpha_shape_3::GENERAL);
        cout << "Optimal alpha: " << *A.find_optimal_alpha(1)<<endl;
        ASTriSet as_triset_ext(&A, Alpha_shape_3::EXTERIOR, "ext");
        WritePly(&as_triset_ext, params);
        ASTriSet as_triset_sing(&A, Alpha_shape_3::SINGULAR, "sing");
        WritePly(&as_triset_sing, params);
        ASTriSet as_triset_reg(&A, Alpha_shape_3::REGULAR, "reg");
        WritePly(&as_triset_reg, params);
        ASTriSet as_triset_int(&A, Alpha_shape_3::INTERIOR, "int");
        WritePly(&as_triset_int, params);
        
        cout << "Creating vertices" << endl;
        map<AsVertexHandle, PriorityVertex3*> vertex_map;
        AsVertexIt vit = A.finite_vertices_begin();
        for(;vit!=A.finite_vertices_end();vit++) //if(A.classify(*vit) != Alpha_shape_3::SINGULAR)
        {
            E.mvp_vertex.push_back(new PriorityVertex3(vit->point().x(),vit->point().y(),vit->point().z()));
            vertex_map.insert(make_pair( vit, E.mvp_vertex.back() ));
        }
        cout << E.mvp_vertex.size() << " vertices created" << endl;
        
        cout << "Creating edges" << endl;
        typedef pair<AsVertexHandle, AsVertexHandle> v_pair_t;
        map<v_pair_t, PriorityEdge3*> edge_map;
        float ref_length_inv = 1.f/params.Get<float>("rbl");
        AsEdgeIt eit = A.finite_edges_begin();
        for(;eit!=A.finite_edges_end();eit++) if(A.classify(*eit) != Alpha_shape_3::EXTERIOR)
        {
			// find the vertices of the edge
			PriorityVertex3 * p_v[2];
			AsVertexHandle vh[2];
			int i[] = {eit->second, eit->third}; // idx of the 2 vertices of the edge
			for(int e=0; e<2; e++)
			{
				p_v[e]=NULL;
				auto v_it = vertex_map.find(eit->first->vertex(i[e]));
				if (v_it != vertex_map.end())
				{
					vh[e] = v_it->first;
					p_v[e] = v_it->second;
				}
			}
			if(p_v[0] == p_v[1]) cout << "WARNING: NULL edge encountered" << endl;
			else
			{	
				float length=(p_v[1]->P - p_v[0]->P).Norm();
				E.mvp_edge.push_back(new PriorityEdge3(length*ref_length_inv));
				v_pair_t vertex_pair = (vh[0] > vh[1] ? make_pair(vh[0],vh[1]) : make_pair(vh[0],vh[1]));
				edge_map.insert(make_pair(vertex_pair, E.mvp_edge.back()));
			}
        }
        cout << E.mvp_edge.size() << " edges created" << endl;
        
        cout << "Creating faces" << endl;
        float ref_area_inv = 1.f/params.Get<float>("ra");
        unsigned n_bad_tri=0;
        AsFacetIt fit = A.finite_facets_begin();
        for(;fit!=A.finite_facets_end();fit++) if(A.classify(*fit) != Alpha_shape_3::EXTERIOR)
        {
			//Cell_handle face = fit->first;
            PriorityVertex3 * p_v[3];
            int v_idx=0;
            // face index fit->second is index of vertex opposite to the face in the tet
            for(int i=0; i<4; i++) if(i != fit->second) 
            {
				p_v[i]=NULL;
				auto v_it = vertex_map.find(fit->first->vertex(i));
				if (v_it != vertex_map.end()) p_v[v_idx++]=v_it->second;
				else cout << "ERROR: can't find a vertex in vertex_map" << endl;
			}
			PriorityEdge3 * p_e[3];
			int e_idx=0;
			// iterate on the 3 pairs of indices not fit->second = 3 edges of the face
            for(int i=0; i<3; i++) if(i != fit->second)
				for(int j=i+1; j<4; j++) if(j != fit->second)
            {
				p_e[e_idx]=NULL;
				auto e_it = edge_map.find(make_pair(fit->first->vertex(i),fit->first->vertex(j)));
				if (e_it != edge_map.end()) p_e[e_idx]=e_it->second;
				else
				{
					e_it = edge_map.find(make_pair(fit->first->vertex(j),fit->first->vertex(i)));
					if (e_it != edge_map.end()) p_e[e_idx]=e_it->second;
					else cout << "ERROR: can't find an edge in edge_map" << endl;
				}
				e_idx++;
			}
			if(e_idx == 3 && p_v[0] != p_v[1] && p_v[0] != p_v[2] && p_v[2] != p_v[1])
			{
				//float weight = ref_area_inv*((p_v[1]->P - p_v[0]->P)^(p_v[2]->P - p_v[0]->P)).Norm(); // area
				float weight = ref_area_inv*A.get_alpha_status(*fit).alpha_min(); // circum radius
				E.mvp_face.push_back(new PriorityFace3(&pqueue,
					p_v[0], p_v[1], p_v[2], p_e[0], p_e[1], p_e[2], weight, true)); // init all on
			}
			else n_bad_tri++;
        }
        cout << E.mvp_face.size() << " faces created " << n_bad_tri << " bad triangles" << endl;
        
        cout << "Adding all faces to the priority queue" << endl;
        // needs to be done after structure is built because priority computation might depend on it
        for(auto & pface_it:E.mvp_face) pface_it->add();
    } // destroy temporaries (Alpha_shape_3, v_pt and the maps)
    
    double E_init = E.E(), E_cur = E_init;   
    cout << "Greedy gradient descent with init" << endl;
    cout << setprecision(10);
    E.Dispay(false);
    unsigned int iter = 0;
    while(pqueue.begin()->first < 0. && iter++ < 2*E.mvp_face.size())
    {
        PriorityFace3 & best_pface = *pqueue.begin()->second;
        //best_pface.priority(true);
        E_cur += pqueue.begin()->first;
        if(iter%1000 == 0)
			cout << "it " << iter << (best_pface.m_on?"-":"+") <<
					" w=" << best_pface.weight() << " pri=" <<
					pqueue.begin()->first << " E=" << E_cur << endl;
        best_pface.switchState();
        // E.Dispay(); // just to check, very costly
    }
    // check energy
    E.Dispay();
    cout << "E_init=" << E_init << endl;
    
    PfaceTriSet pf_triset(E.mvp_face, "kinwa");
    WritePly(&pf_triset, params);

    //vector<bool> v_b(dt.number_of_edges()); //should we keep edge b
    cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;
    return 0;
}


