
#include "PriorityVertex3.h"
#include "PriorityFace3.h"

int PriorityVertex3::onValence() const
{
	// possible optimization: store it and ++ or -- when an adjacent face switches
    int n=0;
    for(auto & p_face:mvp_face) if(p_face->m_on) n++;
    return n;
}

std::ostream& operator<<(std::ostream& os, const PriorityVertex3& v)
{
    os << v.P << '-' << v.onValence() << '/' << v.Valence();
    return os;
}
