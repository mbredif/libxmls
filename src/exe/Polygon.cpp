
#define WHAT "Polygon: extracts planar polygons from a point cloud"

#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <vector>
#include <list>
#include <sstream> // iss
#include <stdlib.h> // atof, atoi, rand(), abs()
#include <string> // c_str(), to_string()
#include <omp.h>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <math.h> // sqrt
#include <random>
#include <algorithm>
#include <stdio.h> //printf
#include "libXMls/XMls.h"
#include "libParam/ParamSet.h"
#include "LiteGeom/LgPoint3.hpp"
#include "LiteGeom/LgPlane3.hpp"
#include "LiteGeom/LgDistance3.hpp"
#include "LiteGeom/LgPoint2.hpp"

// *******CGAL************
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Alpha_shape_2.h>
#include <CGAL/Alpha_shape_vertex_base_2.h>
#include <CGAL/Alpha_shape_face_base_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/algorithm.h>
#include <CGAL/assertions.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Point_3.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K ;
typedef K::FT FT;
typedef K::Point_2 Point2;
typedef K::Point_3 Point3;
typedef K::Segment_2 Segment2;
typedef CGAL::Polygon_2<K> Polygon_2;
typedef CGAL::Alpha_shape_vertex_base_2 <K> Vb;
typedef CGAL::Alpha_shape_face_base_2 <K> Fb;
typedef CGAL:: Triangulation_data_structure_2<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation_2;
typedef CGAL::Alpha_shape_2<Triangulation_2> Alpha_shape_2;
typedef Alpha_shape_2::Alpha_shape_edges_iterator AsEdgeIt;

using namespace std;

class Plane : public Lg::Plane3f
{
public:
    unsigned char rc, gc, bc;
    vector<XEchoIndex> v_inlier_idx;
    Plane(){}
    Plane(Lg::Point3f A, Lg::Point3f B, Lg::Point3f C):
        Lg::Plane3f(A, B, C), rc(128+rand () % 127), gc(128+rand () % 127), bc(128+rand () % 127), v_inlier_idx(0)
    {
        Lg::Plane3f::Normalize();
    }
};

/// All infos relevant for RANSAC. Precompute and access x,y,z coords
class XYZ
{
public:
    XMls * mp_mls;
    XFloatAttrib * mp_x, *mp_y, *mp_z, *mp_refl;
    XShortAttrib * mp_id;
    XUCharAttrib * mp_proc, * mp_r, * mp_g, * mp_b;

    XYZ(XMls * p_mls, ParamSet & param):mp_mls(p_mls)
    {
        mp_x = mp_mls->AddEchoAttrib<XFloatAttrib>("x");
        mp_y = mp_mls->AddEchoAttrib<XFloatAttrib>("y");
        mp_z = mp_mls->AddEchoAttrib<XFloatAttrib>("z");
	mp_refl = mp_mls->GetEchoAttrib<XFloatAttrib>("reflectance");
        mp_id = mp_mls->AddEchoAttrib<XShortAttrib>("id");
        mp_proc = mp_mls->AddEchoAttrib<XUCharAttrib>("processed");
        mp_r = mp_mls->AddEchoAttrib<XUCharAttrib>("r");
        mp_g = mp_mls->AddEchoAttrib<XUCharAttrib>("g");
        mp_b = mp_mls->AddEchoAttrib<XUCharAttrib>("b");
        // pivot point
        XPt3D Pivot(param.Get<double>("pe"), param.Get<double>("pn"), param.Get<double>("ph"));

        for(XEchoIndex echo_idx=0; echo_idx<mp_mls->NEcho(0); echo_idx++)
        {
            XPt3D P = mp_mls->Pworld(0, echo_idx)-Pivot;
            //XPt3D Cw = mls.Cworld(0, echo_idx);
            mp_x->at(0)[echo_idx] = P.X;
            mp_y->at(0)[echo_idx] = P.Y;
            mp_z->at(0)[echo_idx] = P.Z;
            mp_id->at(0)[echo_idx] = -1;
            mp_proc->at(0)[echo_idx] = 0;
            //float e0=Cw.X-param.pivot_E, n0=Cw.Y-param.pivot_N, h0=Cw.Z-param.pivot_H;
        }
        cout << mp_mls->NEcho(0) << " x,y,z coords precomputed" << endl;
    }
    inline unsigned int N(){return mp_mls->NEcho(0);}
    inline Lg::Point3f P(XEchoIndex echo_idx)
    {
        return Lg::Point3f(mp_x->at(0)[echo_idx],mp_y->at(0)[echo_idx],mp_z->at(0)[echo_idx]);
    }
    inline Plane getPlane(XEchoIndex i1, XEchoIndex i2, XEchoIndex i3)
    {
        return Plane(P(i1), P(i2), P(i3));
    }
    inline bool processed(XEchoIndex echo_idx)
    {
        return mp_proc->at(0)[echo_idx]>=0;
    }
    inline bool hasPlane(XEchoIndex echo_idx)
    {
        return mp_id->at(0)[echo_idx]>=0;
    }
    inline short & id(XEchoIndex echo_idx)
    {
        return mp_id->at(0)[echo_idx];
    }
    inline void setRGB(XEchoIndex echo_idx, unsigned char r, unsigned char g, unsigned char b)
    {
      float f = .05*mp_refl->at(0)[echo_idx]+1;
      if(f<0.2) f = 0.2;
      if(f>1.) f = 1.;
        mp_r->at(0)[echo_idx]=f*r;
        mp_g->at(0)[echo_idx]=f*g;
        mp_b->at(0)[echo_idx]=f*b;
    }
    inline unsigned char r(XEchoIndex echo_idx){return mp_r->at(0)[echo_idx];}
    inline unsigned char g(XEchoIndex echo_idx){return mp_g->at(0)[echo_idx];}
    inline unsigned char b(XEchoIndex echo_idx){return mp_b->at(0)[echo_idx];}
};

bool contains(vector<int> & v_idx, int idx)
{
    for(int & i:v_idx) if(i==idx) return true;
    return false;
}

vector<int> getDiffRandIdx(int nIdx, int maxIdx) {
    vector<int> v_idx;
    for(int i=0; i<nIdx; i++)
    {
        int randIdx=0;
        do randIdx = rand() % maxIdx; while(contains(v_idx,randIdx));
        v_idx.push_back(randIdx);
    }
    return v_idx;
}

int RandInterval(int min, int max)
{
    return rand() % (max-min+1) + min;
}

/// gather the indices of unprocessed echoes in the sensor neighborhood of seed_idx
vector<XEchoIndex> getSensorNeighborhood(XYZ & xyz, XEchoIndex seed_idx, int window_width)
{
    vector<XEchoIndex> v_ret;
    XPulseIndex seed_pulse_idx = xyz.mp_mls->IdxPulse(0,seed_idx);
    unsigned int n_pulse = xyz.mp_mls->NPulse(0);
    int ppl = xyz.mp_mls->PulsePerLine();
    for(int dl=-window_width; dl<=window_width; dl++)
    {
        for(int dc=-window_width; dc<=window_width; dc++)
        {
            if(dc != 0 || dl != 0)
            {
                XPulseIndex pulse_idx = seed_pulse_idx+dl*ppl+dc;
                if(pulse_idx>=0 && pulse_idx<n_pulse)
                {
                    XEchoIndex echo_idx = xyz.mp_mls->IdxFirstEcho(0, pulse_idx);
                    for(int i=0; i < xyz.mp_mls->NbOfEcho(0, pulse_idx); i++)
                    {
                        if(!xyz.hasPlane(echo_idx))
                            v_ret.push_back(echo_idx);
                        echo_idx++;
                    }
                }
            }
        }
    }
    return v_ret;
}

template <typename T> void Write(char * & it, T data)
{
    *reinterpret_cast<T*>(it) = data;
    it += sizeof(T);
}

template <> void Write<Lg::Point3f>(char * & it, Lg::Point3f P)
{
    Write<float>(it, P.x());
    Write<float>(it, P.y());
    Write<float>(it, P.z());
}

class AbstractTriSet
{
public:
    AbstractTriSet(){}
    virtual unsigned int Ntri()=0;
    virtual char * Buffer()=0;
    inline unsigned int VertexBufferSize() {return 9*sizeof(float)*Ntri();}
};

class ASTriSet:public AbstractTriSet
{
public:
    Alpha_shape_2 * mp_as;
    unsigned int m_n_tri;
    Lg::Point3f m_O, m_m, m_k;
    ASTriSet(Alpha_shape_2 * p_as, Lg::Point3f O, Lg::Point3f m, Lg::Point3f k):
        mp_as(p_as),m_n_tri(0), m_O(O), m_m(m), m_k(k)
    {
        // we want to export triangles and singular edges
        Alpha_shape_2::Finite_faces_iterator fit = mp_as->finite_faces_begin();
        //for(;fit!=mp_as->finite_faces_end();fit++)
        //    if(mp_as->classify(fit) == Alpha_shape_2::INTERIOR) m_n_tri++;
        Alpha_shape_2::Finite_edges_iterator eit = mp_as->finite_edges_begin();
        for(;eit!=mp_as->finite_edges_end();eit++)
            if(mp_as->classify(*eit) == Alpha_shape_2::REGULAR) m_n_tri++;
    }
    virtual unsigned int Ntri() {return m_n_tri;}
    virtual char * Buffer()
    {
        char * buffer = new char[VertexBufferSize()], * it = buffer;
//         Alpha_shape_2::Finite_faces_iterator fit = mp_as->finite_faces_begin();
//         for(;fit!=mp_as->finite_faces_end();fit++)
//             if(mp_as->classify(fit) == Alpha_shape_2::INTERIOR)
//                 for(int i=0; i<3; i++)
//                 {
//                     auto & P = fit->vertex(i)->point();
//                     Lg::Point3f PP = m_O + P.x()*m_m + P.y()*m_k;
//                     Write<Lg::Point3f>(it, PP);
//                 }
        Alpha_shape_2::Finite_edges_iterator eit = mp_as->finite_edges_begin();
        for(;eit!=mp_as->finite_edges_end();eit++)
        if(mp_as->classify(*eit) == Alpha_shape_2::REGULAR)
        {
	  Segment2 seg = mp_as->segment(*eit);
	  Point2 S = seg.source(), T = seg.target();
	  for(int i=0; i<2; i++) Write<Point2>(it, S);
	  Write<Point2>(it, T);
        }
        return buffer;
    }
};

class VPTriSet:public AbstractTriSet
{
public:
    vector<Lg::Point3f> mv_P;
    VPTriSet(vector<Lg::Point3f> v_P): mv_P(v_P)
    {
        if(mv_P.size()%3)
            cout << "ERROR: non multiple of 3 size in VPTriSet" << endl;
    }
    virtual unsigned int Ntri() {return mv_P.size()/3;}
    virtual char * Buffer()
    {
        char * buffer = new char[VertexBufferSize()], * it = buffer;
        for(auto & i_P:mv_P)
            Write<Lg::Point3f>(it, i_P);
        return buffer;
    }
};

typedef vector< pair<Lg::Point3f, Lg::Point3f> > v_seg_t;
class VSTriSet:public AbstractTriSet
{
public:
    v_seg_t * mpv_seg;
    VSTriSet(v_seg_t * pv_seg):mpv_seg(pv_seg)
    {}
    virtual unsigned int Ntri() {return mpv_seg->size();}
    virtual char * Buffer()
    {
        char * buffer = new char[VertexBufferSize()], * it = buffer;
        for(auto & seg:*mpv_seg)
        {
            for(int i=0; i<2; i++) Write<Lg::Point3f>(it, seg.first);
            Write<Lg::Point3f>(it, seg.second);
        }
        return buffer;
    }
};

void writePly(AbstractTriSet * p_triset, ParamSet & param, string suffix)
{
    string filemame = param.Get<string>("o")+suffix+".ply";
    ofstream fileOut(filemame.c_str());
    if(!fileOut.good())
    {
        cout << "Cannot open " + filemame + " for writing\n";
        return;
    }

    // write text header
    unsigned int n_face = p_triset->Ntri(), n_vertex = 3*n_face;
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << param.Get<string>("ept") << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "element face " << n_face << endl;
    fileOut << "property list uchar int vertex_indices" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    char * buffer = p_triset->Buffer();
    unsigned int vertex_buffer_size = p_triset->VertexBufferSize();
    cout << "Writing " << n_vertex << " vertices of size " <<
            vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    // triangle list
    unsigned int tri_bytesize = sizeof(unsigned char)+3*sizeof(int);
    unsigned long tri_buffer_size = tri_bytesize*n_face;
    buffer = new char[tri_buffer_size];
    char * it = buffer;
    for(unsigned int tri_idx=0; tri_idx<n_face; tri_idx++)
    {
        Write<unsigned char>(it, 3);
        for(int i=0; i<3; i++) Write<int>(it, 3*tri_idx+i);
    }
    cout << "Writing " << n_face << " triangles of size " <<
            tri_buffer_size << "=" << 1.e-6*tri_buffer_size << "MB" << endl;
    cout << "Total " << 1.e-6*(p_triset->VertexBufferSize()+tri_buffer_size) << "MB" << endl;
    fileOut.write(buffer, tri_buffer_size);
    fileOut.close();
}

void writePly(XYZ & xyz, ParamSet & param)
{
    string filename = param.Get<string>("o")+".xyz.ply";
    ofstream fileOut(filename);
    if(!fileOut.good())
    {
        cout << "Cannot open " + filename + " for writing\n";
        return;
    }

    // write text header
    unsigned int n_vertex = xyz.N();
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated from " << param.Get<string>("ept") << endl;
    fileOut << "element vertex " << n_vertex << endl;
    fileOut << "property float x" << endl;
    fileOut << "property float y" << endl;
    fileOut << "property float z" << endl;
    fileOut << "property uchar red" << endl;
    fileOut << "property uchar green" << endl;
    fileOut << "property uchar blue" << endl;
    fileOut << "end_header" << endl;
    // vertex list
    unsigned int vertex_size = 3*sizeof(float)+3*sizeof(unsigned char);
    unsigned int vertex_buffer_size = n_vertex*vertex_size;
    char * buffer = new char[vertex_buffer_size], * it = buffer;
    for(XEchoIndex echo_idx=0; echo_idx<n_vertex; echo_idx++)
    {
        Write<Lg::Point3f>(it, xyz.P(echo_idx));
        Write<unsigned char>(it, xyz.r(echo_idx));
        Write<unsigned char>(it, xyz.g(echo_idx));
        Write<unsigned char>(it, xyz.b(echo_idx));
    }
    cout << "Writing " << n_vertex << " vertices of size " <<
            vertex_buffer_size << "=" << 1.e-6*vertex_buffer_size << "MB" << endl;
    fileOut.write(buffer, vertex_buffer_size);
    delete buffer;
    fileOut.close();
}

/// return a vector orthogonal to n by choosing the longest of n^x and n^y
/// returns (0,0,0) if n is (0,0,0)
Lg::Point3f best_m(Lg::Point3f n)
{
    if(abs(n.x())>abs(n.y()))
        return Lg::Point3f(-n.z(),0,n.x());
    return Lg::Point3f(0,n.z(),-n.y());
}

template <class OutputIterator>
void alpha_edges( const Alpha_shape_2& A, OutputIterator out)
{
    for (AsEdgeIt alpha_it = A.Alpha_shape_edges_begin(),
         end = A.Alpha_shape_edges_end();
         alpha_it!=end; ++alpha_it){
        *out++ = A.segment(*alpha_it);
    }
}

v_seg_t alpha_shape_edge( vector<Plane> const & Planes, XYZ & xyz, double radius)
{
    v_seg_t ret;
    for(auto const& pl : Planes)
    {
        /// choose a local frame for the plane
        Lg::Point3f n = pl.Normal();
        Lg::Point3f m = best_m(n);
        m.Normalize();
        Lg::Point3f k = n^m;
        k.Normalize();

        vector<K::Point_2> v_pt2(pl.v_inlier_idx.size());
        Lg::Point3f O = pl.Point();
        int i=0;
        for(auto & inlier_idx:pl.v_inlier_idx)
        {
            Lg::Point3f V = xyz.P(inlier_idx)-O;
            v_pt2[i++] = K::Point_2(V*m, V*k);
        }
        Alpha_shape_2 A(v_pt2.begin(), v_pt2.end(),
                        FT(radius),
                        Alpha_shape_2::REGULARIZED);

        vector<Segment2> v_segment;
        alpha_edges(A, back_inserter(v_segment));
        cout << "Alpha Shape computed" << endl;
        cout << v_segment.size() << " alpha shape edges" << endl;
        cout << "Optimal alpha: " << *A.find_optimal_alpha(1)<<endl;

        for(auto & seg:v_segment)
        {
            Lg::Point3f A,B;
            A = O + seg.source().x()*m + seg.source().y()*k;
            B = O + seg.target().x()*m + seg.target().y()*k;
            ret.push_back(make_pair(A,B));
        }
    }
    return ret;
}

vector<Lg::Point3f> alpha_shape_tri( vector<Plane> const & Planes, XYZ & xyz, double radius)
{
    vector<Lg::Point3f> ret;
    for(auto const& pl : Planes)
    {
        /// choose a local frame for the plane
        Lg::Point3f n = pl.Normal();
        Lg::Point3f m = best_m(n);
        m.Normalize();
        Lg::Point3f k = n^m;
        k.Normalize();

        vector<K::Point_2> v_pt2(pl.v_inlier_idx.size());
        Lg::Point3f O = pl.Point();
        int i=0;
        for(auto & inlier_idx:pl.v_inlier_idx)
        {
            Lg::Point3f V = xyz.P(inlier_idx)-O;
            v_pt2[i++] = K::Point_2(V*m, V*k);
        }
        Alpha_shape_2 A(v_pt2.begin(), v_pt2.end(),
                        FT(radius),
                        Alpha_shape_2::REGULARIZED);

        Alpha_shape_2::Finite_faces_iterator fit = A.finite_faces_begin();
        for(;fit!=A.finite_faces_end();fit++)
            if(A.classify(fit) == Alpha_shape_2::INTERIOR)
                for(int i=0; i<3; i++)
                {
                    auto & P = fit->vertex(i)->point();
                    ret.push_back(O + P.x()*m + P.y()*k);
                }
        cout << "Alpha Shape computed" << endl;
        cout << "Optimal alpha: " << *A.find_optimal_alpha(1)<<endl;
    }
    cout << ret.size()/3 << " alpha shape triangles" << endl;
    return ret;
}

int main(int argc, char **argv)
{
    cout << WHAT << endl;
    ParamSet param;
    XMls::AddParam(param); // adds all params to create a XMLS
    param.Add<string>("o", "test", "Base name for all output files");
    param.Add<int>("fbi", -1, "id of the first block to process (look at info.txt in ept_dir for valid range)");
    param.Add<int>("lbi", -1, "id of the last block to process (inclusive)");
    param.Add<unsigned>("sww", 20, "sample_window_width: width of the RANSAC sampling window, 0 for infinite");
    param.Add<unsigned>("gww", 4, "growing_window_width: width of the window used for region growing, 0 for infinite");
    param.Add<float>("it", 0.1f, "inlier_threshold: threshold separating plane in/outliers");
    param.Add<float>("mp", 0.01f, "miss_probability: probability that a remaining primitive will be missed. If 0, used fixed n instead");
    param.Add<float>("asr", 0.5f, "alpha_shape_radius to generate a polygon from inliers of each plane");
    param.Add<unsigned>("mps", 30000, "min_poly_size: minimum number of inliers for a polygon");
    param.Add<float>("mp", 0.01, "miss_probability: number of RANSAC iteration is computed to ensure probability to miss a minimum primitive is below this");
    param.Add<unsigned>("n", 500, "number of RANSAC iterations if mp=0");
    param.Add<double>("pe", 0., "pivot_E");
    param.Add<double>("pn", 0., "pivot_N");
    param.Add<double>("ph", 0., "pivot_H");

    if(argc < 2) {cout << param.Help() << endl; return 0;}
    param.Parse(argc, argv);
    cout << param.Display() << endl;

    int start_s = clock();
    srand(time(NULL));

    // constructor and infos accessible after construction
    XTrajecto traj(param);
    XMls mls(param, &traj);
    XSecond first_block_idx(param.Get<int>("fbi"));
    XSecond last_block_idx(param.Get<int>("lbi"));
    mls.Select(first_block_idx, last_block_idx);
    cout << mls.NBlock() << " block(s) selected" << endl;
    for(unsigned i_block=0; i_block<mls.NBlock(); i_block++)
    {
        mls.Load(i_block); // load the first (and only) block
        // if pivot was not set, make one by rounding first traj point
        if(param.Get<double>("pe")==0. && param.Get<double>("pn")==0)
        {
            XPt3D Pivot = traj.GetGeoref(0).Translation();
            param.Set<double>("pe", 100.*(int)(Pivot.X/100));
            param.Set<double>("pn", 100.*(int)(Pivot.Y/100));
        }

        XYZ xyz(&mls, param); // structure to precompute and access x,y,z coords in local frame

        vector<Plane> v_plane;
        unsigned sample_window_width = param.Get<unsigned>("sww");
	unsigned growing_window_width = param.Get<unsigned>("gww");
        unsigned min_plane_size = param.Get<unsigned>("mps");
	float miss_probability = param.Get<float>("mp");
	unsigned n_total_inlier=0;
	unsigned n_total_iter=0;
        float inlier_threshold = param.Get<float>("it");
        do { // inner RANSAC iteration on extracted planar polygons
            cout << "PLANE " << v_plane.size() << endl;
            Plane best_plane;
	    unsigned i_ransac = 0, n_ransac = param.Get<unsigned>("n");
            do // inner RANSAC iterations = plane sampling
            {
                if(i_ransac%10==0) cout << i_ransac <<" "<< best_plane.v_inlier_idx.size() << endl;
		vector<XEchoIndex> v_sample_idx(3,0); // Indices of the 3 samples for plane RANSAC
		if(sample_window_width==0) // sample in all the cloud
		{
		  for(int i=0; i<3; i++) do v_sample_idx[i] = rand()%xyz.N(); while(xyz.hasPlane(v_sample_idx[0]));
		}
		else // sample in neighborhood only
		{
		  vector<XEchoIndex> v_neigh;
		  do
		  {
		      do v_sample_idx[0] = rand()%xyz.N(); while(xyz.hasPlane(v_sample_idx[0]));
		      v_neigh = getSensorNeighborhood(xyz, v_sample_idx[0], sample_window_width);
		  } while(v_neigh.size()<5);
		  vector<int> v_idx = getDiffRandIdx(3, v_neigh.size());
		  for(int i=0; i<3; i++) v_sample_idx[i] = v_neigh[v_idx[i]];
		}
		
                Plane cur_plane = xyz.getPlane(v_sample_idx[0], v_sample_idx[1], v_sample_idx[2]);
		
		// inlier selection
                vector<XEchoIndex> v_inlier_idx;
                if(growing_window_width==0) // plane distance only
                {
                    for(XEchoIndex echo_idx=0; echo_idx<xyz.N(); echo_idx++)
                        if(!xyz.hasPlane(echo_idx) &&
                                abs(Lg::SignedDistance(xyz.P(echo_idx), cur_plane)) < inlier_threshold)
                            v_inlier_idx.push_back(echo_idx);
                }
                else // region growing
                {
		  XEchoIndex echo_idx = v_sample_idx[0];
		  list<XEchoIndex> pile;
		  vector<XEchoIndex> to_revert;
		  pile.push_back(echo_idx);
		  xyz.id(echo_idx) = -2; // processed tag, -1 is unattributed, >=0 attributed to a plane
		  to_revert.push_back(echo_idx);
		  while (!pile.empty())
		  {
		    echo_idx = pile.back();
		    pile.pop_back();
		    if (abs(Lg::SignedDistance(xyz.P(echo_idx), cur_plane)) < inlier_threshold)
		    {
		      v_inlier_idx.push_back(echo_idx);
		      vector<XEchoIndex> v_neigh=getSensorNeighborhood(xyz, echo_idx, growing_window_width);
		      for(auto & neigh_idx:v_neigh)
		      {
			if(xyz.id(neigh_idx) == -1) // unprocessed and unattributed
			{
			  pile.push_back(neigh_idx);
			  xyz.id(neigh_idx)=-2;
			  to_revert.push_back(neigh_idx);
			}
		      }
		    }
		  }
		  // avoid iterating over the whole cloud to reset processed state
		  for(auto & idx:to_revert) xyz.id(idx)=-1;
		}
		if (v_inlier_idx.size() > best_plane.v_inlier_idx.size())
		{
		  best_plane = cur_plane;
		  best_plane.v_inlier_idx = v_inlier_idx;
		  //cout << "Best region inlier: " << v_inlier_idx.size() << endl;
		  if(miss_probability>0.f)
		  {
		    unsigned n_pts = xyz.N()-n_total_inlier;
		    // minimum number of points of an interesting primitive
		    unsigned n_min = max(min_plane_size, (unsigned)best_plane.v_inlier_idx.size());
		    unsigned n_miss = log(miss_probability)/log(1-(float)n_min/(float)(4*n_pts));
		    cout << "n_miss = " << n_miss << endl;
		    if(n_miss < n_ransac) n_ransac = n_miss;
		  }
		}
	    } while(i_ransac++ < n_ransac);
	    cout << "Best region total inlier: " << best_plane.v_inlier_idx.size() << endl;
	    n_total_inlier += best_plane.v_inlier_idx.size();
	    v_plane.push_back(best_plane);
	    n_total_iter += n_ransac;
	    
	    for (XEchoIndex & echo_idx:best_plane.v_inlier_idx)
	    {
	      xyz.id(echo_idx) = v_plane.size();
	      xyz.setRGB(echo_idx, best_plane.rc, best_plane.gc, best_plane.bc);
	    }
	}
        while (v_plane.back().v_inlier_idx.size() > min_plane_size); // boucle principale RANSAC
	int stop_s = clock();
	cout << "PRANSAC found " << n_total_inlier << " inliers in " <<  v_plane.size() << " polygon(s) in "
	  << n_total_iter << " iterations " << (stop_s-start_s)/double(CLOCKS_PER_SEC) << "s" << endl;

        // Output of RANSAC with (Sensor Topology or Region Growing)
        writePly(xyz, param);

        // alpha shapes
        v_seg_t v_seg = alpha_shape_edge(v_plane, xyz, param.Get<float>("asr"));
        VSTriSet triset(&v_seg);
        //vector<Lg::Point3f> v_P = alpha_shape_tri(v_plane, xyz, param.Get<float>("asr"));
        //VPTriSet triset(v_P);
        writePly(&triset, param, ".mesh");
        mls.Free(i_block); // free block
    }
    int stop_s = clock();
    cout << "EXECUTION TIME:  " << (stop_s-start_s)/double(CLOCKS_PER_SEC) << endl;
    return 0;
}
