
#define WHAT "RieglFlexport: flexible Riegl exporter from echo/pulse tables (ept)"

#include <ctime>
#include <iostream>
#include <fstream>
#include <set>
#include "libXMls/XMls.h"
//#include "libXBase/XArchiXMLException.h"

#define COUT_SUB 1000000

using namespace std;

int Typesize(string type_name)
{
    if(type_name == "float32" || type_name == "int32" || type_name == "uint32") return 4;
    if(type_name == "float64" || type_name == "int64" || type_name == "uint64") return 8;
    if(type_name == "int8" || type_name == "uint8") return 1;
    if(type_name == "int16" || type_name == "uint16") return 2;
    return 0;
}

struct attrib_meta_info_t
{
    string name, type;
    unsigned int bytesize;
    bool required;
    attrib_meta_info_t(string name_, string type_):name(name_), type(type_), bytesize(Typesize(type_)), required(false){}
};

typedef vector<attrib_meta_info_t> v_attrib_info_t;

inline void AddAttribInfo(v_attrib_info_t & v_info, string attrib_name, string type_name)
{
    v_info.push_back(attrib_meta_info_t(attrib_name, type_name));
}

v_attrib_info_t ExportableAttributes()
{
    // order matters ! if you add new attribs, compute them in the same order
    v_attrib_info_t v_attrib_info;
    // preffered but non mandatory in decreasing type size order
    // if you add new attribs not at end update indices and boolean computation before main loop
    // 0
    AddAttribInfo(v_attrib_info, "GPS_time", "float64");
    AddAttribInfo(v_attrib_info, "range", "float32");
    AddAttribInfo(v_attrib_info, "theta", "float32");
    AddAttribInfo(v_attrib_info, "phi", "float32");

    // 4
    AddAttribInfo(v_attrib_info, "x_sensor", "float32");
    AddAttribInfo(v_attrib_info, "y_sensor", "float32");
    AddAttribInfo(v_attrib_info, "z_sensor", "float32");

    // 7
    AddAttribInfo(v_attrib_info, "x_origin", "float32");
    AddAttribInfo(v_attrib_info, "y_origin", "float32");
    AddAttribInfo(v_attrib_info, "z_origin", "float32");

    // 10
    AddAttribInfo(v_attrib_info, "x_ins", "float32");
    AddAttribInfo(v_attrib_info, "y_ins", "float32");
    AddAttribInfo(v_attrib_info, "z_ins", "float32");

    // 13
    AddAttribInfo(v_attrib_info, "x", "float32");
    AddAttribInfo(v_attrib_info, "y", "float32");
    AddAttribInfo(v_attrib_info, "z", "float32");

    // 16
    AddAttribInfo(v_attrib_info, "xVelocity", "float32");
    AddAttribInfo(v_attrib_info, "yVelocity", "float32");
    AddAttribInfo(v_attrib_info, "zVelocity", "float32");

    // 19
    AddAttribInfo(v_attrib_info, "roll", "float32");
    AddAttribInfo(v_attrib_info, "pitch", "float32");
    AddAttribInfo(v_attrib_info, "plateformHeading", "float32");
    AddAttribInfo(v_attrib_info, "wanderAngle", "float32");

    // 23
    AddAttribInfo(v_attrib_info, "xAcceleration", "float32");
    AddAttribInfo(v_attrib_info, "yAcceleration", "float32");
    AddAttribInfo(v_attrib_info, "zAcceleration", "float32");

    // 26
    AddAttribInfo(v_attrib_info, "xBodyAngularRate", "float32");
    AddAttribInfo(v_attrib_info, "yBodyAngularRate", "float32");
    AddAttribInfo(v_attrib_info, "zBodyAngularRate", "float32");

    // 29
    AddAttribInfo(v_attrib_info, "northPositionRMSError", "float32");
    AddAttribInfo(v_attrib_info, "eastPositionRMSError", "float32");
    AddAttribInfo(v_attrib_info, "downPositionRMSError", "float32");

    // 32
    AddAttribInfo(v_attrib_info, "northVelocityRMSError", "float32");
    AddAttribInfo(v_attrib_info, "eastVelocityRMSError", "float32");
    AddAttribInfo(v_attrib_info, "downVelocityRMSError", "float32");

    // 35
    AddAttribInfo(v_attrib_info, "RollRMSError", "float32");
    AddAttribInfo(v_attrib_info, "PitchRMSError", "float32");
    AddAttribInfo(v_attrib_info, "headingRMSError", "float32");

    // 38
    AddAttribInfo(v_attrib_info, "amplitude", "float32");
    AddAttribInfo(v_attrib_info, "reflectance", "float32");

    // 40
    AddAttribInfo(v_attrib_info, "deviation", "uint8");
    AddAttribInfo(v_attrib_info, "nb_of_echo", "uint8");
    AddAttribInfo(v_attrib_info, "num_echo", "uint8");

    return v_attrib_info;
}

// not implemented as a map for 2 reasons:
// - We need to ensure the attributes ordering
// - efficiency in critical loop
inline bool Require(v_attrib_info_t & v_attrib_info, string name)
{
    if(name.empty()) return false;
    for(v_attrib_info_t::iterator it=v_attrib_info.begin(); it!=v_attrib_info.end(); it++)
    {
        if(it->name == name)
        {
            it->required = true;
            return true;
        }
    }
    if(name[0]!='(') cout << "Warning: Required unknown attribute " << name << endl;
    return false;
}

// return true if at least one attribute in the range is required
inline bool Requires(v_attrib_info_t & v_attrib_info, int i_start, int i_end)
{
    for(int i=i_start; i<i_end; i++) if(v_attrib_info[i].required) return true;
    return false;
}

template <typename T> void Write(char * & it, T data)
{
    *reinterpret_cast<T*>(it) = data;
    it += sizeof(T);
}

//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    cout << WHAT << endl;
    v_attrib_info_t v_attrib_info = ExportableAttributes();
    ParamSet params;
    XMls::AddParam(params);
    params.Add<string>("o", "test.ply", "name of the output ply file (should end with .ply)");
    params.Add<string>("a", "attrib.txt", "name of the text file with the names of all attributes to export (cf list below)");
    params.Add<int>("is", -1, "i_start: start time of the points to export, -1 for first ept block");
    params.Add<int>("ie", -1, "i_end: end time of the points to export, -1 for last ept block");
    params.Add<double>("pe", 0., "pivot_E: optionally set manually the pivot point, 0. for automatic");
    params.Add<double>("pn", 0., "pivot_N: optionally set manually the pivot point");
    if(argc<2)
    {
      cout << params.Help() << endl;
      for(v_attrib_info_t::iterator it=v_attrib_info.begin(); it!=v_attrib_info.end(); it++)
      {
	  cout << "(" << it->type << ") " << it->name << endl;
      }
      cout << "The most simple is to copy paste the lines above in attrib.txt file and keep only the lines you need" << endl;
      return 0;
    }
    params.Parse(argc, argv);
    XSecond i_start=params.Get<int>("is"), i_end=params.Get<int>("ie");
    string attrib_filename = params.Get<string>("a");
    string out_name = params.Get<string>("o");

    // read attrib file
    ifstream attrib_file(attrib_filename);
    int n_attrib = 0;
    do
    {
        string attrib; attrib_file >> attrib;
        cout << "att " << n_attrib << " " << attrib << endl;
        if(Require(v_attrib_info, attrib)) n_attrib++;
    } while(!attrib_file.eof());
    cout << attrib_filename << " selects " << n_attrib << " known attributes" << endl;

    // read all echo/pulse tables in time interval
    clock_t start = clock();
    XTrajecto traj(params);
    XMls mls(params, &traj);
    cout << mls.NPulseAttrib() << "/" << mls.NEchoAttrib() << " echos/pulses attributes found in "
      << params.Get<string>("ept") << endl;

    mls.Select(i_start, i_end);
    cout << mls.NBlock() << " block(s) selected" << endl;

    if(params.Get<double>("pe") == 0.)
    {
      XPt3D Pivot = traj.GetGeoref(0).Translation();
      Pivot.Z=0; // Lidarformat does not support it, we don't need a pivot in Z anyways
      params.Set<double>("pe", 100*(int)(Pivot.X/100));
      params.Set<double>("pn", 100*(int)(Pivot.Y/100));
    }
    XPt3D Pivot(params.Get<double>("pe"), params.Get<double>("pn"), 0);

    // iteration on echos
    start = clock();
    ofstream fileOut(out_name.c_str());
    if(!fileOut.good())
    {
        cout << "Cannot open " + out_name + " for writing\n";
        return 3;
    }

    // write text header
    fileOut << "ply\nformat binary_little_endian 1.0" << endl;
    fileOut << "comment Generated with RieglFlexport" << endl;
    fileOut << "comment IGN offset Pos " << Pivot.X << " " << Pivot.Y << " 0" << endl;
    fileOut << "comment IGN offset Time " << mls.m_ept_time_pivot.ToString() << endl;
    fileOut << "comment IGN EPT " << params.Get<string>("ept") << endl;
    fileOut << "comment IGN sbet " << params.Get<string>("sbet") << endl;
    if(params.Get<string>("acc").size()>0)
      fileOut << "comment IGN acc " << params.Get<string>("acc") << endl;
    fileOut << "comment IGN calib " << params.Get<string>("calib") << endl;
    fileOut << "element vertex " << mls.NTotalEcho() << endl;
    int echo_bytesize = 0;
    for(v_attrib_info_t::iterator it=v_attrib_info.begin(); it!=v_attrib_info.end(); it++) if(it->required)
    {
        fileOut << "property " << it->type << " " << it->name << endl;
        echo_bytesize += it->bytesize;
    }
    fileOut << "end_header" << endl;
    // precompute booleans to know what computation blocks are needed (for efficiency)
    bool requires_xyz_sensor = Requires(v_attrib_info, 4, 7);
    bool requires_xyz_origin = Requires(v_attrib_info, 7, 10);
    bool requires_xyz_ins = Requires(v_attrib_info, 10, 13);
    bool requires_xyz = Requires(v_attrib_info, 13, 16);
    bool requires_sbet = Requires(v_attrib_info, 16, 29);
    bool requires_accuracy = Requires(v_attrib_info, 29, 38);

    // gather the required attribs
    XFloatAttrib * p_ampl = NULL, * p_refl = NULL;
    XUCharAttrib * p_dev = NULL;
    if(v_attrib_info[38].required) p_ampl = mls.GetEchoAttrib<XFloatAttrib>("amplitude");
    if(v_attrib_info[39].required) p_refl = mls.GetEchoAttrib<XFloatAttrib>("reflectance");
    if(v_attrib_info[40].required) p_dev = mls.GetEchoAttrib<XUCharAttrib>("deviation");
    // TODO: authorize export of any attribute in ept_dir

    unsigned long buffer_size = echo_bytesize * mls.NTotalEcho();
    cout << "Buffer size: " << echo_bytesize << " * " << mls.NTotalEcho() <<
            " = " << buffer_size << "=" << buffer_size/(1024*1024) << "MB" << endl;
    char * buffer = new char[buffer_size], * it = buffer;
    cout.precision(16);
    bool compare = false;
    double max_error = 0.;
    for(XBlockIndex block_idx=0; block_idx<mls.NBlock(); block_idx++)
    {
        mls.Load(block_idx);
        for(XEchoIndex i_echo=0; i_echo<mls.NEcho(block_idx); i_echo++)
        {
            bool display_echo = (i_echo == 0);
            // time
            double time = mls.Time(block_idx, i_echo);
            if(v_attrib_info[0].required) Write<double>(it, time);

            // spherical coords
            if(v_attrib_info[1].required) Write<float>(it, mls.Range(block_idx, i_echo));
            if(v_attrib_info[2].required) Write<float>(it, mls.Theta(block_idx, i_echo));
            if(v_attrib_info[3].required) Write<float>(it, mls.Phi(block_idx, i_echo));
            if(display_echo)
            {
                cout << "["<< 100*block_idx/mls.NBlock() << "%] Time:" << time << endl;
                display_echo = false; // comment this line for more infos displayed
            }

            // euclidian coords
            if(requires_xyz_sensor)
            {
                XPt3D Pt_sensor = mls.P(block_idx, i_echo);
                if(v_attrib_info[4].required) Write<float>(it, Pt_sensor.X);
                if(v_attrib_info[5].required) Write<float>(it, Pt_sensor.Y);
                if(v_attrib_info[6].required) Write<float>(it, Pt_sensor.Z);
                if(display_echo) cout << "Pt_sensor: " << Pt_sensor << endl;
            }
            if(requires_xyz_origin)
            {
                XPt3D Pt_origin = mls.Cworld(block_idx, i_echo)-Pivot;
                if(v_attrib_info[7].required) Write<float>(it, Pt_origin.X);
                if(v_attrib_info[8].required) Write<float>(it, Pt_origin.Y);
                if(v_attrib_info[9].required) Write<float>(it, Pt_origin.Z);
                if(display_echo) cout << "Pt_origin: " << Pt_origin << endl;
            }
            if(requires_xyz_ins)
            {
                XPt3D Pt_ins = mls.Oworld(block_idx, i_echo)-Pivot;
                if(v_attrib_info[10].required) Write<float>(it, Pt_ins.X);
                if(v_attrib_info[11].required) Write<float>(it, Pt_ins.Y);
                if(v_attrib_info[12].required) Write<float>(it, Pt_ins.Z);
                if(display_echo) cout << "Pt_ins:" << Pt_ins << endl;
            }
            if(requires_xyz)
            {
                XPt3D Pt_ground = mls.Pworld(block_idx, i_echo)-Pivot;
                if(v_attrib_info[13].required) Write<float>(it, Pt_ground.X);
                if(v_attrib_info[14].required) Write<float>(it, Pt_ground.Y);
                if(v_attrib_info[15].required) Write<float>(it, Pt_ground.Z);
                if(display_echo) cout << "Pt_ground: " << Pt_ground << endl;
                if(compare)
                {
                    XPt3D Pt_ground_frame = mls.Pworld_interpol_angles(block_idx, i_echo);
                    XPt3D err = Pt_ground - Pt_ground_frame;
                    double err_norm = sqrt(err.X*err.X + err.Y*err.Y + err.Z*err.Z);
                    if(err_norm > max_error) max_error = err_norm;
                    if(display_echo)
                    {
                        cout << "Pt_ground_frame: " << Pt_ground_frame << endl;
                        cout << "Error: " << err_norm << endl;
                    }
                }
            }
            // rest of the sbet
            if(requires_sbet)
            {
                SbetEvent sbet_event = mls.Sbet(block_idx, i_echo);
                if(v_attrib_info[16].required) Write<float>(it, sbet_event.m_xVelocity);
                if(v_attrib_info[17].required) Write<float>(it, sbet_event.m_yVelocity);
                if(v_attrib_info[18].required) Write<float>(it, sbet_event.m_zVelocity);
                if(v_attrib_info[19].required) Write<float>(it, sbet_event.m_roll);
                if(v_attrib_info[20].required) Write<float>(it, sbet_event.m_pitch);
                if(v_attrib_info[21].required) Write<float>(it, sbet_event.m_plateformHeading);
                if(v_attrib_info[22].required) Write<float>(it, sbet_event.m_wanderAngle);
                if(v_attrib_info[23].required) Write<float>(it, sbet_event.m_xAcceleration);
                if(v_attrib_info[24].required) Write<float>(it, sbet_event.m_yAcceleration);
                if(v_attrib_info[25].required) Write<float>(it, sbet_event.m_zAcceleration);
                if(v_attrib_info[26].required) Write<float>(it, sbet_event.m_xBodyAngularRate);
                if(v_attrib_info[27].required) Write<float>(it, sbet_event.m_yBodyAngularRate);
                if(v_attrib_info[28].required) Write<float>(it, sbet_event.m_zBodyAngularRate);
                if(display_echo) cout << "Sbet:" << sbet_event << endl;
            }
            // uncertainties
            if(requires_accuracy)
            {
                AccuracyEvent acc_event = mls.Accuracy(block_idx, i_echo);
                if(v_attrib_info[29].required) Write<float>(it, acc_event.m_northPositionRMSError);
                if(v_attrib_info[30].required) Write<float>(it, acc_event.m_eastPositionRMSError);
                if(v_attrib_info[31].required) Write<float>(it, acc_event.m_downPositionRMSError);
                if(v_attrib_info[32].required) Write<float>(it, acc_event.m_northVelocityRMSError);
                if(v_attrib_info[33].required) Write<float>(it, acc_event.m_eastVelocityRMSError);
                if(v_attrib_info[34].required) Write<float>(it, acc_event.m_downVelocityRMSError);
                if(v_attrib_info[35].required) Write<float>(it, acc_event.m_RollRMSError);
                if(v_attrib_info[36].required) Write<float>(it, acc_event.m_PitchRMSError);
                if(v_attrib_info[37].required) Write<float>(it, acc_event.m_headingRMSError);
                if(display_echo)
                {
                    cout << "Acc:" << acc_event << endl;
                }
            }
            // physical
            if(v_attrib_info[38].required) Write<float>(it, p_ampl->at(block_idx)[i_echo]);
            if(v_attrib_info[39].required) Write<float>(it, p_refl->at(block_idx)[i_echo]);
            if(v_attrib_info[40].required) Write<unsigned char>(it, p_dev->at(block_idx)[i_echo]);
            if(v_attrib_info[41].required) Write<unsigned char>(it, mls.NumEcho(block_idx, i_echo));
            if(v_attrib_info[42].required) Write<unsigned char>(it, mls.NbOfEcho(block_idx, i_echo));
            if(display_echo)
            {
                if(v_attrib_info[38].required) cout << "Ampl:" << p_ampl->at(block_idx)[i_echo];
                if(v_attrib_info[39].required) cout << "|Refl:" << p_refl->at(block_idx)[i_echo];
                if(v_attrib_info[40].required) cout << "|Dev:" << p_dev->at(block_idx)[i_echo];
                cout << "|" << mls.NumEcho(block_idx, i_echo) << "/" << mls.NbOfEcho(block_idx, i_echo) << endl;
            }
        }
        mls.Free(block_idx);
    }
    if(compare) cout << "max_error: " << max_error << endl;
    cout << "Writing " << mls.NTotalEcho() << " echos of size " << echo_bytesize << "=" << buffer_size/1000000 << "MB to " << out_name << endl;
    fileOut.write(buffer, buffer_size); // todo: split buffer if too big
    fileOut.close();
    delete buffer;
    cout << "Done in " << (double)(clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl;
    return 0;
}

