# libXMLS README
Mobile Laser Scanning library by Bruno Vallet for IGN-France

Required dependencies: TinyXML, Boost (date_time system filesystem)

Optional dependencies: Ori CGAL

Test:

Unzip data.zip at the root

Assuming your build dir is next to the root, go to it and:

./bin/XMlsInfo ../libxmls/data/traj/ 20140616 ../libxmls/data/Calib.xml ../libxmls/data/test.ept

the output should end with:

Range in [2.124,446.8]

Amplitude in [0.56,70.98]

Reflectance in [-20.29,38.2]

Deviation in [0,255]

numEcho in [0,3]

X in [-30.14,103.6]

Y in [-446.5,413.4]

Z in [-2.096,1.175e-38]

Xw in [6.508e+05,6.516e+05]

Yw in [6.861e+06,6.862e+06]

Zw in [-56.51,75.76]


